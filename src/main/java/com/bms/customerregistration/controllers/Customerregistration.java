package com.bms.customerregistration.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bms.customerregistration.service.Customerservice;

import net.sf.json.JSONObject;

@RestController
@RequestMapping("api/v1/cust")
public class Customerregistration {
	
	@Autowired
	private Customerservice cs;
	
	@PostMapping(value="login", consumes = "application/json", produces = "application/json")
	public Map<String, Object> validatecustomer(@RequestBody JSONObject body) {
	  return cs.verifyUser(body);
	}
	
	@PostMapping(value="registration", consumes = "application/json")
	public Boolean customerregister(@RequestBody JSONObject body) throws Exception {
      return cs.registerUser(body);
    }
	
	@GetMapping
	public String testexception() {
		
		throw new NullPointerException();
		
	}

}
