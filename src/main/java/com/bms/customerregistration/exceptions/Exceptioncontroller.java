package com.bms.customerregistration.exceptions;

import java.util.HashMap;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class Exceptioncontroller  extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(value = {Exception.class})
	protected HashMap<String, Object> customerexception(Exception e, WebRequest request){
		HashMap<String, Object> map = new HashMap<>();
		map.put("msg",e.getMessage());
		map.put("timestamp", java.time.LocalDateTime.now());
		map.put("REQ_SATAUS", HttpStatus.BAD_REQUEST);
		return map;
	}

}
