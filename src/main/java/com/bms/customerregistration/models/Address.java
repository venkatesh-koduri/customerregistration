package com.bms.customerregistration.models;

import javax.persistence.Embeddable;

import com.bms.customerregistration.entity.Customer;


@Embeddable
public class Address extends Customer{
	
	private String state;
	private String country;
	private String email;
	private String pan;
	private String contactno;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}
	public String getContactno() {
		return contactno;
	}
	public void setContactno(String contactno) {
		this.contactno = contactno;
	}
	
	

}
