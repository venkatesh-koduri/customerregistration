package com.bms.customerregistration.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.bms.customerregistration.entity.Customer;

@Repository
public interface Customerrepository extends CrudRepository<Customer, Integer> {
	
	boolean existsByUsernameAndPassword(String username, String password);
}
