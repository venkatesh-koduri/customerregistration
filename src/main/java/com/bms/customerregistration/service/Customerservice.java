package com.bms.customerregistration.service;

import java.util.Map;

import org.springframework.stereotype.Component;

import net.sf.json.JSONObject;


@Component
public interface Customerservice {

	Map<String, Object> verifyUser(JSONObject body);
	Boolean registerUser(JSONObject body) throws Exception;
}
