package com.bms.customerregistration.serviceimpl;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bms.customerregistration.entity.Customer;
import com.bms.customerregistration.repositories.Customerrepository;
import com.bms.customerregistration.service.Customerservice;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.json.JSONObject;

@Service
public class Customeserviceimpl implements Customerservice {
	
	@Autowired
	private Customerrepository custrep;

	@Override
	public Map<String, Object> verifyUser(JSONObject body) {
      Map<String, Object> retmap = new HashMap<>();
      retmap.put("status", false);
      retmap.put("msg", "user not found");
	  if(custrep.existsByUsernameAndPassword(body.getString("username"), body.getString("userid"))) {
		  retmap.put("status", true);
		  retmap.put("msg", "user logged in");
	  }
	  return retmap;
	}

	@Override
	public Boolean registerUser(JSONObject body) throws Exception {
		
		boolean issucess = true;
		try {
			com.bms.customerregistration.models.Customer model = new ObjectMapper().readValue(body.toString(), com.bms.customerregistration.models.Customer.class);
			Customer entity = prepareentity(model);
			custrep.save(entity);
		} catch (JsonProcessingException e) {
			issucess = false;
			throw new Exception("Json precess Exception");
		}
		return issucess;
	}

	private Customer prepareentity(com.bms.customerregistration.models.Customer model) {
		Customer entity = new Customer();
		entity.setAccounttype(model.getAccounttype());
		entity.setAddress(model.getAddress());
		entity.setDob(convertStrtoldate(model.getDob()));
		entity.setName(model.getName());
		entity.setPassword(model.getPassword());
		entity.setUsername(model.getUsername());
		return entity;
	}

	private LocalDate convertStrtoldate(String dob) {
		return  LocalDate.of(Integer.valueOf(dob.substring(6)), Integer.valueOf(dob.substring(3,5)), Integer.valueOf(dob.substring(0,2)));
	}	
	
}
