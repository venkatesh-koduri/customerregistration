package com.bms.customerregistration.controllers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;
import java.util.Map;
import com.bms.customerregistration.entity.Customer;
import com.bms.customerregistration.repositories.Customerrepository;
import com.bms.customerregistration.serviceimpl.Customeserviceimpl;

import net.sf.json.JSONObject;


class CustomerregistrationTest {
	
	@InjectMocks
	private Customeserviceimpl impl;
	
	@Mock
	private Customerrepository repo;
	
	@BeforeEach
	public void init() {
	  MockitoAnnotations.openMocks(this);
	}
	
	@Test
	public void validatecustomertest() {
		when(repo.existsByUsernameAndPassword("xyz", "reeee")).thenReturn(false);
		JSONObject jo = new JSONObject();
		jo.put("username", "test");
		jo.put("userid", "test");
	    Map<String, Object> testmap = impl.verifyUser(jo);
		assertEquals(false, testmap.get("status"));
	}
	
	
	@Test
	public void registeruserTest() throws Exception {
		
		when(repo.save(new Customer())).thenReturn(new Customer());
		assertEquals(true, impl.registerUser(testobj()));

	}

	@Test()
	private JSONObject testobj() {
		JSONObject jo = new JSONObject();
		JSONObject adress = new JSONObject();
		adress.put("state","AP");
		adress.put("country","INDIA");
		adress.put("email","abc@gmail.com");
		adress.put("pan","CTRY1245W");
		adress.put("contactno","9784563210");
		jo.put("id", 0);
		jo.put("name","venkatesh");
		jo.put("username","vk");
		jo.put("password","MTIzNDU=");
		jo.put("address", adress);
		jo.put("dob","28-03-2021");
		jo.put("accounttype","Savings");
		return jo;
	}
	
	
	@Test()
	public void registerUserTestException() {
		when(repo.save(new Customer())).thenReturn(new Customer());
		JSONObject jo = testobj();
		jo.put("id", "dfdfddf");
		Exception exp = assertThrows(Exception.class, () -> impl.registerUser(jo));
		assertEquals("Json precess Exception", exp.getMessage());
	}

}
